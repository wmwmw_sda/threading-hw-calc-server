from time import sleep
from random import randint

from sanic import Sanic
from sanic import response as res

app = Sanic(__name__)


@app.route("/api/<operation>/", methods=["POST"])
async def test(req, operation):
    if not req.json:
        return res.json({"error": "empty body"}, status=400)

    if not ("first" in req.json and "second" in req.json):
        return res.json({"error": "Missing first and/or second parameter."}, status=400)

    try:
        x, y = int(req.json["first"]), int(req.json["second"])
    except (ValueError, TypeError):
        return res.json({"error": "parameters must be integer"})

    sleep(randint(500, 2000) / 100)

    if operation == "add":
        result = x + y
    elif operation == "mul":
        result = x * y
    elif operation == "sub":
        result = x - y
    else:
        return res.json({"error": "Invalid operation"}, status=400)

    return res.json({"result": result})


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000)
