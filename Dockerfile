FROM python

RUN pip install poetry
COPY ./poetry.lock ./pyproject.toml /tmp/

WORKDIR /tmp/
RUN poetry config virtualenvs.create false \
    && poetry install

WORKDIR /app
COPY main.py ./

CMD ["python", "main.py"]